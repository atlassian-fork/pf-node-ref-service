import {iContext} from '../lib/context';
import config from '../config';

export default () => {
  return async (ctx: iContext, next: Function) => {
    ctx.config = config;
    await next();
  };
};
