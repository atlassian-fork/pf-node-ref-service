/*globals it, describe, beforeEach, afterEach */

import * as assert from 'assert';
import config from './config';
import {iContext} from '../lib/context';

const next = async () => Promise.resolve();

describe('middleware/config', () => {
  it('should add config to context', () => {
    let ctx:any = {};

    return config()((ctx as iContext), next)
      .then(() => {
        assert.ok(ctx.config);
      });
  });
});
