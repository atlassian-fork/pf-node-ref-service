/*globals it, describe, beforeEach, afterEach */

import * as assert from 'assert';
import cors from './cors';
import {iContext} from '../lib/context';

const next = async () => Promise.resolve();

describe('middleware/cors', () => {
  let headers:{[key: string]: string | string[]} = {};

  const ctx = {
    config: {
      cors: {
        origin: '*'
      }
    },
    fresh: false,
    status: 0,
    set: (field: string | any, val?: string | string[]) => {
      if (val) {
        headers[field] = val;
        return;
      }
      Object.keys(field).forEach((fieldName) => {
        headers[fieldName] = field[fieldName];
      });
    },
    get: (field: string) => {
      return headers[field];
    }
  };

  beforeEach(() => {
    headers = {};
  });

  it('should set cache headers to default of no options given', () => {
    const promise = cors();
    ctx.set('access-control-request-headers','GET');
    ctx.set('origin', 'test');

    return promise((ctx as iContext), next)
      .then(() => {
        delete headers['access-control-request-headers'];
        delete headers['origin'];
        assert.deepEqual(headers, {
          'Access-Control-Allow-Headers': 'GET',
          'Access-Control-Allow-Origin': 'test',
          'Access-Control-Allow-Methods': 'GET,HEAD,PUT,POST,DELETE',
          'Access-Control-Allow-Credentials': 'true'
        });
      });
  });

  it('should honor origin option', () => {
    const promise = cors({origin: 'optorigin'});
    ctx.set('access-control-request-headers','GET');
    return promise((ctx as iContext), next)
      .then(() => {
        delete headers['access-control-request-headers'];
        delete headers['origin'];
        assert.deepEqual(headers, {
          'Access-Control-Allow-Headers': 'GET',
          'Access-Control-Allow-Origin': 'optorigin',
          'Access-Control-Allow-Methods': 'GET,HEAD,PUT,POST,DELETE',
          'Access-Control-Allow-Credentials': 'true'
        });
      });
  });
});
