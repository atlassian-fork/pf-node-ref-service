import {iContext} from '../lib/context';
import * as _ from 'lodash';

interface CacheHandlerOpts {
  'max-age'?: number;
  'no-store'?: boolean;
  'no-cache'?: boolean;
}

export default (opts: CacheHandlerOpts = {}) => {
  _.defaults(opts, {
    'max-age': 5,
    'no-store': false,
    'no-cache': false
  });
  return async (ctx: iContext, next: Function) => {

    await next();

    const ccHeader: string[] = [
      'private'
    ];

    if (opts['no-store']) {
      ccHeader.push('no-store');
    }

    if (opts['no-cache']) {
      ccHeader.push('no-cache');
    }

    if (opts['max-age']) {
      ccHeader.push('max-age=' + opts['max-age']);
    }

    ctx.set('Cache-Control', ccHeader.join(','));

    // check if the etag has changed
    if (ctx.fresh) {
      ctx.status = 304;
    }
  };
};
