/*globals it, describe, beforeEach, afterEach */

import * as assert from 'assert';
import cacheHandler from './cachehandler';
import {iContext} from '../lib/context';

const next = async () => Promise.resolve();

describe('middleware/cachehandler', () => {
  let headers:{[key: string]: string | string[]} = {};

  const ctx = {
    fresh: false,
    status: 0,
    set: (field: string, val: string | string[]) => {
      headers[field] = val;
    }
  };

  beforeEach(() => {
    headers = {};
  });

  it('should set cache headers to default of no options given', () => {
    const promise = cacheHandler();

    return promise((ctx as iContext), next)
      .then(() => {
        assert.deepEqual(headers, {
          'Cache-Control': 'private,max-age=5'
        });
      });
  });

  it('should honor the no-store options', () => {
    const promise = cacheHandler({'no-store': true});

    return promise((ctx as iContext), next)
      .then(() => {
        assert.deepEqual(headers, {
          'Cache-Control': 'private,no-store,max-age=5'
        });
      });
  });

  it('should honor the no-cache options', () => {
    const promise = cacheHandler({'no-cache': true});

    return promise((ctx as iContext), next)
      .then(() => {
        assert.deepEqual(headers, {
          'Cache-Control': 'private,no-cache,max-age=5'
        });
      });
  });

  it('should honor the max-age options', () => {
    const promise = cacheHandler({'max-age': 9999});

    return promise((ctx as iContext), next)
      .then(() => {
        assert.deepEqual(headers, {
          'Cache-Control': 'private,max-age=9999'
        });
      });
  });

  it('should return 304 if ctx is fresh', () => {
    const promise = cacheHandler({'max-age': 9999});

    ctx.fresh = true;

    return promise((ctx as iContext), next)
      .then(() => {
        assert.equal(ctx.status, 304);
        assert.deepEqual(headers, {
          'Cache-Control': 'private,max-age=9999'
        });
      });
  });

});
