/*globals it, describe, beforeEach, afterEach */

import * as assert from 'assert';
import Metrics from './metrics';

describe('lib/metrics', () => {

  let metrics: Metrics;

  beforeEach(() => {
    metrics = new Metrics('mastercomponent');
  });

  describe('#child', () => {

    it('returns a childobject of Metrics', () => {
      const child = metrics.child();
      assert.ok(child instanceof Metrics);
    });

    it('accepts a component name', () => {
      const child = metrics.child('componentname');
      assert.equal(child.componentName, 'componentname');
    });

    it('accepts a component name as array', () => {
      const child = metrics.child(['componentname', 'test']);
      assert.equal(child.componentName, 'componentname.test');
    });

    it('propagates events to the parent', (done) => {

      const child = metrics.child('childcomponent');

      const eventTimeout = setTimeout(() => {
        done(new Error('Event never fired'));
      }, 10);

      metrics.on('gauge', (metricname: string, data: any) => {
        clearTimeout(eventTimeout);
        assert.equal(metricname, 'mastercomponent.childcomponent.metricname');
        assert.deepEqual(data, {
          value: 12345,
          sampleRate: 100,
          tags: ['tag1', 'tag2']
        });
        done();
      });

      child.gauge('metricname', 12345, 100, ['tag1', 'tag2']);
    });

    it('propagates events to the parent, including prefixes', (done) => {

      const child = metrics.child('childcomponent', {prefix: 'prefix'});

      const eventTimeout = setTimeout(() => {
        done(new Error('Event never fired'));
      }, 10);

      metrics.on('gauge', (metricname: string, data: any) => {
        clearTimeout(eventTimeout);
        assert.equal(metricname, 'mastercomponent.prefix.childcomponent.metricname');
        assert.deepEqual(data, {
          value: 12345,
          sampleRate: 100,
          tags: ['tag1', 'tag2']
        });
        done();
      });

      child.gauge('metricname', 12345, 100, ['tag1', 'tag2']);
    });

  });

  describe('#getComponentPath', () => {

    it('returns the master componentpath if no childname is given', () => {
      const result = metrics.getComponentPath();
      assert.equal(result, 'mastercomponent');
    });

    it('returns combined componentpath if childname (string) is given', () => {
      const result = metrics.getComponentPath('childcomponent');
      assert.equal(result, 'mastercomponent.childcomponent');
    });

    it('returns combined componentpath if childname (array) is given', () => {
      const result = metrics.getComponentPath(['childcomponent1', 'childcomponent2']);
      assert.equal(result, 'mastercomponent.childcomponent1.childcomponent2');
    });

    it('honors prefixes', () => {
      metrics.prefix = 'prefix';
      const result = metrics.getComponentPath();
      assert.equal(result, 'prefix.mastercomponent');
    });

  });

  describe('#event', () => {

    it('should emit a valid "event" event', (done) => {

      const eventTimeout = setTimeout(() => {
        done(new Error('Event never fired'));
      }, 10);

      metrics.on('event', (name: string, data: any) => {
        clearTimeout(eventTimeout);
        assert.ok(data.ts);
        assert.equal(name, 'mastercomponent');
        assert.equal(data.name, 'eventname');
        assert.deepEqual(data.data, {mydata: true});
        done();
      });

      metrics.event('eventname', {mydata: true});
    });

  });

  describe('#createAndStartTimer', () => {

    it('should return a valid timer object', () => {
      const timer = metrics.createAndStartTimer('metricname', 100, ['tag1', 'tag2']);
      assert.ok(timer);
      assert.ok(timer.emitTimingStat);
      assert.ok(timer.timePromise);
    });

  });

  describe('timer', () => {

    describe('#emitTimingStat', () => {

      it('should emit a valid "timing" event', function (done) {
        this.timeout(10);

        metrics.on('timing', (metricname: string, data: any) => {
          assert.equal(metricname, 'mastercomponent.metricname');
          assert.ok(data.value >= 3 && data.value < 10);
          assert.ok(data.sampleRate, '100');
          assert.deepEqual(data.tags, ['tag1', 'tag2']);
          done();
        });

        const timer = metrics.createAndStartTimer('metricname', 100, ['tag1', 'tag2']);

        setTimeout(() => {
          timer.emitTimingStat();
        }, 3);

      });

    });

    describe('#timePromise', () => {

      it('should emit a "timing" event when the promise resolves', (done) => {
        metrics.on('timing', (metricname: string, data: any) => {
          assert.equal(metricname, 'mastercomponent.metricname');
          assert.ok(data.value >= 3 && data.value < 10);
          assert.equal(data.sampleRate, '100');
          assert.deepEqual(data.tags, ['tag1', 'tag2']);
          done();
        });

        const timer = metrics.createAndStartTimer('metricname', 100, ['tag1', 'tag2']);

        timer.timePromise(new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve();
          }, 3);
        }))
        .catch((e: any) => done(e));
      });

      it('should emit a "timing" event when the promise rejects', (done) => {
        metrics.on('timing', (metricname: string, data: any) => {
          assert.equal(metricname, 'mastercomponent.metricname');
          assert.ok(data.value >= 3 && data.value < 10);
          assert.equal(data.sampleRate, '100');
          assert.deepEqual(data.tags, ['tag1', 'tag2']);
          done();
        });

        const timer = metrics.createAndStartTimer('metricname', 100, ['tag1', 'tag2']);

        timer.timePromise(new Promise((resolve, reject) => {
          setTimeout(() => {
            reject(new Error());
          }, 5);
        }));
      });
    });
  });

  describe('#timing', () => {

    it('should emit a valid "timing" event', (done) => {
      const eventTimeout = setTimeout(() => {
        done(new Error('Event never fired'));
      }, 10);

      metrics.on('timing', (metricname: string, data: any) => {
        clearTimeout(eventTimeout);
        assert.equal(metricname, 'mastercomponent.metricname');
        assert.deepEqual(data, {
          value: 12345,
          sampleRate: 100,
          tags: ['tag1', 'tag2']
        });
        done();
      });

      metrics.timing('metricname', 12345, 100, ['tag1', 'tag2']);
    });

  });
  describe('#increment', () => {

    it('should emit a valid "increment" event', (done) => {
      const eventTimeout = setTimeout(() => {
        done(new Error('Event never fired'));
      }, 10);

      metrics.on('increment', (metricname: string, data: any) => {
        clearTimeout(eventTimeout);
        assert.equal(metricname, 'mastercomponent.metricname');
        assert.deepEqual(data, {
          value: 12345,
          sampleRate: 100,
          tags: ['tag1', 'tag2']
        });
        done();
      });

      metrics.increment('metricname', 12345, 100, ['tag1', 'tag2']);
    });
  });

  describe('#decrement', () => {

    it('should emit a valid "decrement" event', (done) => {
      const eventTimeout = setTimeout(() => {
        done(new Error('Event never fired'));
      }, 10);

      metrics.on('decrement', (metricname: string, data: any) => {
        clearTimeout(eventTimeout);
        assert.equal(metricname, 'mastercomponent.metricname');
        assert.deepEqual(data, {
          value: 12345,
          sampleRate: 100,
          tags: ['tag1', 'tag2']
        });
        done();
      });

      metrics.decrement('metricname', 12345, 100, ['tag1', 'tag2']);
    });
  });

  describe('#histogram', () => {

    it('should emit a valid "histogram" event', (done) => {
      const eventTimeout = setTimeout(() => {
        done(new Error('Event never fired'));
      }, 10);

      metrics.on('histogram', (metricname: string, data: any) => {
        clearTimeout(eventTimeout);
        assert.equal(metricname, 'mastercomponent.metricname');
        assert.deepEqual(data, {
          value: 12345,
          sampleRate: 100,
          tags: ['tag1', 'tag2']
        });
        done();
      });

      metrics.histogram('metricname', 12345, 100, ['tag1', 'tag2']);
    });
  });

  describe('#gauge', () => {

    it('should emit a valid "gauge" event', (done) => {
      const eventTimeout = setTimeout(() => {
        done(new Error('Event never fired'));
      }, 10);

      metrics.on('gauge', (metricname: string, data: any) => {
        clearTimeout(eventTimeout);
        assert.equal(metricname, 'mastercomponent.metricname');
        assert.deepEqual(data, {
          value: 12345,
          sampleRate: 100,
          tags: ['tag1', 'tag2']
        });
        done();
      });

      metrics.gauge('metricname', 12345, 100, ['tag1', 'tag2']);
    });
  });

  describe('#set', () => {

    it('should emit a valid "set" event', (done) => {
      const eventTimeout = setTimeout(() => {
        done(new Error('Event never fired'));
      }, 10);

      metrics.on('set', (metricname: string, data: any) => {
        clearTimeout(eventTimeout);
        assert.equal(metricname, 'mastercomponent.metricname');
        assert.deepEqual(data, {
          value: 12345,
          sampleRate: 100,
          tags: ['tag1', 'tag2']
        });
        done();
      });

      metrics.set('metricname', 12345, 100, ['tag1', 'tag2']);
    });
  });

  describe('#set/get prefix', () => {
    it('sets/get the prefix', () => {
      metrics.prefix = 'test';
      assert.equal(metrics.prefix, 'test');
    });
  });

  describe('#promiseTimer', () => {
    it('measures runtime of resolved promises', (done) => {
      metrics.on('timing', (metricName: string, data: any) => {
        assert.equal(metricName, 'mastercomponent.metricname');
        assert.ok(data.value);
        done();
      });

      const timer = metrics.promiseTimer('metricname');
      timer(new Promise((resolve, reject) => {
        setTimeout(() => resolve(), 100);
      }))
      .catch((e: any) => done(e));
    });
  });

});
