const jwtAuthentication = require('jwt-authentication');

interface SessionValidatorOpts {
  publicKeyBaseUrl: string;
  audience: string;
  subjects: string[];
};

export type tValidateResult = {
  sessionId: string;
  sub: string;
  refreshTimeout: number;
  timedOut: boolean;
}

export class SessionValidator {

  private _jwtValidator: any;
  private _subjects: string[];

  constructor(opts: SessionValidatorOpts) {
    this._subjects = opts.subjects;
    this._jwtValidator = jwtAuthentication.server.create({
      publicKeyBaseUrl: opts.publicKeyBaseUrl,
      resourceServerAudience: opts.audience,
      ignoreMaxLifeTime: true
    });
  }

  async validate(token: string) {
    return new Promise<tValidateResult>((resolve, reject) => {
      this._jwtValidator.validate(token, this._subjects, (err: Error, res: any) => {
        if (err) {
          return reject(err);
        }
        resolve({
          sessionId: res.sessionId,
          sub: res.sub,
          refreshTimeout: res.refreshTimeout,
          timedOut: SessionValidator.isTimedOut(res.refreshTimeout)
        });
      });
    });
  }

  static isTimedOut(timeoutValue: number) {
    return (
      !timeoutValue ||
      isNaN(timeoutValue) ||
      timeoutValue <= Math.floor(Date.now() / 1000)
    );
  }
}
