/*globals it, describe, beforeEach, afterEach */

import * as assert from 'assert';
import * as Bunyan from 'bunyan';
import createLogger from './logger';

describe('lib/logger', () => {
  it('should return an instance of logger', () => {
    const logger = createLogger({ level: 'info' });
    assert.ok(logger instanceof (Bunyan as any));
  });

  it('should honor the "level" option', () => {
    const logger = createLogger({ level: 'debug' });
    assert.ok(logger instanceof (Bunyan as any));
    assert.equal((logger as any)._level, 20);
  });

});
