/*globals it, describe, beforeEach, afterEach */

import * as assert from 'assert';
import {SessionValidator} from './sessionValidator';

describe('lib/sessionValidator', () => {

  describe('class', () => {
    describe('#isTimedOut', () => {
      it('should return false if input > current TS', () => {
        const currentTs = Math.floor(Date.now() / 1000);
        const timedOut = SessionValidator.isTimedOut(currentTs + 1000);
        assert.equal(timedOut, false);
      });

      it('should return true if input <= current TS', () => {
        const timedOut = SessionValidator.isTimedOut(1000);
        assert.equal(timedOut, true);
      });
    });
  });

  describe('instance', () => {
    let validator: SessionValidator;

    beforeEach(() => {
      const opts = {
        publicKeyBaseUrl: 'http://localhost/',
        audience: 'test',
        subjects: ['test']
      };
      validator = new SessionValidator(opts);
    });

  });

});
