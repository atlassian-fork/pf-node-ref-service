import {EventEmitter} from 'events';

/**
 * @class Metrics
 * @param {string|Object} componentName
 * @param {Object|string} config
 * @param {Object} childConfig
 */
export default class Metrics extends EventEmitter {

  private _parent: any;
  private _componentName: string|string[];
  private _config: any;
  private _prefix: string;

  constructor (componentName?: any, config?: any, childConfig?: any) {
    super();

    // generating child
    if (childConfig) {
      this._parent = componentName;
      this._componentName = config;
      this._config = childConfig;
    } else {
      this._componentName = componentName;
      this._config = config || {};
    }

    this._prefix = this._config.prefix;

    if (this._componentName instanceof Array) {
      this._componentName = this._componentName.join('.');
    }
  }

  get componentName () {
    return this._componentName;
  }

  get prefix () {
    return this._prefix;
  }

  set prefix (prefix: string) {
    this._prefix = prefix;
  }

  child (componentName?: string|string[], config?: any) {
    if (!componentName) {
      return this;
    }
    return new Metrics(this, componentName, config || {});
  }

  getComponentPath (childComponentName?: any) {
    const componentPath = [this._componentName];

    if (childComponentName) {
      if (childComponentName instanceof Array) {
        childComponentName = childComponentName.join('.');
      }
      componentPath.push(childComponentName);
    }

    if (this._prefix) {
      componentPath.unshift(this._prefix);
    }

    return componentPath.filter(el => el).join('.');
  }

  _emit (name: string, data: any, childComponentName?: any) {
    const componentPath = this.getComponentPath(childComponentName);

    // We have a parent
    if (this._parent) {
      return this._parent._emit(name, data, componentPath);
    }
    this.emit('metric', name, componentPath, data);
    this.emit(name, componentPath, data);
  }

  event (eventName: string, eventData: any) {
    this._emit('event', {
      ts: new Date().getTime(),
      name: eventName,
      data: eventData
    });
  }

  /**
   * @typedef Metrics~Timer
   * @property {function} emitTimingStat - emits timing stats with the amount of time elapsed since the
   * @property {function} timePromise - emits timing stats when the promise resolves or rejects
   * creation of this timer object
   */

  /**
   * Creates a timer object and start the timing
   * @param stat {String|Array} The stat(s) to send
   * @param sampleRate {Number=} The Number of times to sample (0 to 1). Optional.
   * @param tags {Array=} The Array of tags to add to metrics. Optional.
   * @return {Metrics~Timer}
   */
  createAndStartTimer (stat: string, sampleRate?: number, tags?: string[]) {
    const metrics = this;
    let startTime: number = new Date().getTime();;
    return {
      timePromise: function <T>(promise: Promise<any>): Promise<T> {
        return promise
          .then((value: T) => { this.emitTimingStat(); return value; })
          .catch((error: Error) => { this.emitTimingStat(); throw error; });
      },
      emitTimingStat: function () {
        const currentTime = new Date().getTime();
        metrics.timing(stat, currentTime - startTime, sampleRate, tags);
      }
    };
  }

  /**
   * Creates a promise timer function
   * @param stat {String|Array} The stat(s) to send
   * @param sampleRate {Number=} The Number of times to sample (0 to 1). Optional.
   * @param tags {Array=} The Array of tags to add to metrics. Optional.
   * @return {Metrics~Timer}
   * @memberOf Metrics
   */
  promiseTimer (stat: string, sampleRate?: number, tags?: string[]) {
    let startTime: number = 0;

    const emitTimingStat = () => {
      const currentTime = new Date().getTime();
      this.timing(stat, currentTime - startTime, sampleRate, tags);
    };

    return function <T>(promise: Promise<any>): Promise<T> {
      startTime = new Date().getTime();
      return promise
        .then((value: T) => { emitTimingStat(); return value; })
        .catch((error: Error) => { emitTimingStat(); throw error; });
    };
  }

  /**
   * Represents the timing stat
   * @param stat {String|Array} The stat(s) to send
   * @param time {Number} The time in milliseconds to send
   * @param sampleRate {Number=} The Number of times to sample (0 to 1). Optional.
   * @param tags {Array=} The Array of tags to add to metrics. Optional.
   */
  timing (stat: string, time: number, sampleRate?: number, tags?: string[]) {
    this.logMetric<number>('timing', stat, time, sampleRate, tags);
  }

  /**
   * Increments a stat by a specified amount
   * @param stat {String|Array} The stat(s) to send
   * @param value The value to send
   * @param sampleRate {Number=} The Number of times to sample (0 to 1). Optional.
   * @param tags {Array=} The Array of tags to add to metrics. Optional.
   */
  increment (stat: string, value?: number, sampleRate?: number, tags?: string[]) {
    this.logMetric<number>('increment', stat, value, sampleRate, tags);
  }


  /**
   * Decrements a stat by a specified amount
   * @param stat {String|Array} The stat(s) to send
   * @param value The value to send
   * @param sampleRate {Number=} The Number of times to sample (0 to 1). Optional.
   * @param tags {Array=} The Array of tags to add to metrics. Optional.
   */
  decrement (stat: string, value?: number, sampleRate?: number, tags?: string[]) {
    this.logMetric<number>('decrement', stat, value, sampleRate, tags);
  }

  /**
   * Represents the histogram stat
   * @param stat {String|Array} The stat(s) to send
   * @param value The value to send
   * @param sampleRate {Number=} The Number of times to sample (0 to 1). Optional.
   * @param tags {Array=} The Array of tags to add to metrics. Optional.
   */
  histogram (stat: string, value: number, sampleRate?: number, tags?: string[]) {
    this.logMetric<number>('histogram', stat, value, sampleRate, tags);
  }


  /**
   * Gauges a stat by a specified amount
   * @param stat {String|Array} The stat(s) to send
   * @param value The value to send
   * @param sampleRate {Number=} The Number of times to sample (0 to 1). Optional.
   * @param tags {Array=} The Array of tags to add to metrics. Optional.
   */
  gauge (stat: string, value: number, sampleRate?: number, tags?: string[]) {
    this.logMetric<number>('gauge', stat, value, sampleRate, tags);
  }

  /**
   * Counts unique values by a specified amount
   * @param stat {String|Array} The stat(s) to send
   * @param value The value to send
   * @param sampleRate {Number=} The Number of times to sample (0 to 1). Optional.
   * @param tags {Array=} The Array of tags to add to metrics. Optional.
   */
  set (stat: string, value: number, sampleRate?: number, tags?: string[]) {
    this.logMetric<number>('set', stat, value, sampleRate, tags);
  }


  /**
   * Logs metric of a specific type
   * @param type {String} The metric type
   * @param stat {String|Array} The stat(s) to send
   * @param value The value to send
   * @param sampleRate {Number=} The Number of times to sample (0 to 1). Optional.
   * @param tags {Array=} The Array of tags to add to metrics. Optional.
   */
  logMetric<T> (type: string, stat: string, value?: T, sampleRate?: number, tags?: string[]) {
    this._emit(type, {
      value: value,
      sampleRate: sampleRate,
      tags: tags
    }, stat);
  }

};
