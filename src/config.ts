import {normalize} from 'path';

export type tASAPConfig = {
  issuer: string;
  audience: string;
  privateKey: string;
  keyId?: string;
}

export type tSessionService = {
  cookieName: string;
  asapAudience?: string;
  cookieDomain: string;
  interactor: tInteractor;
  validator: {
    keyserver: string;
    audience: string;
  };
}

type tInteractor = {
  baseUrl: string;
  asapAudience?: string;
  timeout?: number;
}

type tCorsConfig = {
  origin: string;
}

type tLoggerConfig = {
  defaultLevel: string;
  responseTimeLimit: number;
}

export interface IConfig {
  port?: number;
  asap?: tASAPConfig;
  sessionService?: tSessionService;
  cors?: tCorsConfig;
  logger?: tLoggerConfig;
  requesttracer?: {
    traceheader: string;
  };
};

function buildConfig(): IConfig {
  const config: IConfig = require('etc')()
    .reverse()
    .file(normalize(__dirname + '/../config.default.json'))
    .file(normalize(__dirname + '/../config.local.json'))
    .env('ppld_', '_', JSON.parse)
    .toJSON();

  config.asap.privateKey = process.env['ASAP_PRIVATE_KEY'] || config.asap.privateKey;
  config.asap.issuer = process.env['ASAP_ISSUER'] || config.asap.issuer;
  config.asap.audience = process.env['ASAP_AUDIENCE'] || config.asap.audience;

  return config;
}

export default buildConfig();
