
export function basic(router:any, action:any) {
  router
    .get('/health', action.health)
    .get('/', action.index);

  return router.routes();
}
