/*globals it, describe, beforeEach, afterEach */

import {AxiosError} from 'axios';
import * as assert from 'assert';
import * as sessionService from './sessionService';
import * as nock from 'nock';

const validateResponses: {[key: string]: any[]} = {
  valid_token: [
    200,
    'abcdef',
    {
      'cache-control': 'no-cache',
      'content-type': 'application/jwt',
      date: 'Tue, 24 Jan 2017 23:48:34 GMT',
      'content-length': '6',
      connection: 'Close'
    }
  ],
  invalid_token: [
    400,
    {
      error: {
        key: 'token.decode.error',
        message: 'Couldn\'t parse the given session jwt'
      }
    }, {
      'content-type': 'application/json;charset=UTF-8'
    }
  ],
  missing_token: [
    400,
    {
      error: {
        key: 'validateSession.invalidParam',
        message: '\'sessionToken\' parameter can\'t be a blank string'
      }
    }, {
      'content-type': 'application/json;charset=UTF-8'
    }
  ],
  session_notfound: [
    401,
    {
      error: {
        key: 'validateSession.sessionNotFound',
        message: 'sessionNotFound'
      }
    }, {
      'content-type': 'application/json;charset=UTF-8'
    }
  ],
  session_expired: [
    401,
    {
      error: {
        key: 'validateSession.sessionExpired',
        message: 'sessionExpired'
      }
    }, {
      'content-type': 'application/json;charset=UTF-8'
    }
  ],

  invalid_asap_key: [
    401,
    {
      timestamp: 1485304118535,
      status: 401,
      error: 'Unauthorized',
      message: 'Authentication Failed: Failed to retrieve public key',
      path: '/validate'
    }, {
      'content-type': 'application/json;charset=UTF-8'
    }
  ]
};



describe('interactors/sessionService', () => {

  describe('@SessionNotFoundError', () => {
    it('should return an instance of Error', () => {
      const test = new sessionService.SessionNotFoundError('name', (new Error() as AxiosError));
      assert.ok(test instanceof Error, 'Not an instance of Error');
    });

    it('name should be "SessionNotFoundError"', () => {
      const test = new sessionService.SessionNotFoundError('name', (new Error() as AxiosError));
      assert.equal(test.name, 'SessionNotFoundError');
    });
  });

  describe('@SessionExpiredError', () => {
    it('should return an instance of Error', () => {
      const test = new sessionService.SessionExpiredError('name', (new Error() as AxiosError));
      assert.ok(test instanceof Error, 'Not an instance of Error');
    });

    it('name should be "SessionExpiredError"', () => {
      const test = new sessionService.SessionExpiredError('name', (new Error() as AxiosError));
      assert.equal(test.name, 'SessionExpiredError');
    });
  });

  describe('@InvalidTokenError', () => {
    it('should return an instance of Error', () => {
      const test = new sessionService.InvalidTokenError('name', (new Error() as AxiosError));
      assert.ok(test instanceof Error, 'Not an instance of Error');
    });

    it('name should be "InvalidTokenError"', () => {
      const test = new sessionService.InvalidTokenError('name', (new Error() as AxiosError));
      assert.equal(test.name, 'InvalidTokenError');
    });
  });

  describe('@SessionServiceInteractor', () => {

    describe('#constructor', () => {
      it('should return an instance', () => {
        const opts:sessionService.SessionServiceOptions = {
          baseUrl: 'http://testurl'
        };
        const ss = new sessionService.SessionServiceInteractor(opts);
        assert.ok(ss);
      });
    });


    describe('#validate', () => {
      let service: sessionService.SessionServiceInteractor;
      const opts:sessionService.SessionServiceOptions = {
        baseUrl: 'http://test.io',
        authorization: 'none'
      };

      const mockValidate = (mockType: string) => {
        return nock(opts.baseUrl)
          .post('/validate')
          .reply(function(uri, requestBody) {
            return validateResponses[mockType];
          });
      };

      beforeEach(() => {
        nock.cleanAll();
        service = new sessionService.SessionServiceInteractor(opts);
      });

      it('should return a new key if current token is valid', () => {
        mockValidate('valid_token');
        return service.validate('validtoken')
          .then((result: any) => {
            assert.equal(result, 'abcdef');
          });
      });

      it('should throw InvalidTokenError if current token is missing', () => {
        mockValidate('missing_token');
        return service.validate('')
          .then((result: any) => {
            throw new Error('Promise was unexpectedly fulfilled. Result: ' + result);
          })
          .catch((e: Error) => {
            assert.equal(e.name, 'InvalidTokenError');
            assert.equal(e.message, '\'sessionToken\' parameter can\'t be a blank string');
          });
      });

      it('should throw InvalidTokenError if current token is invalid', () => {
        mockValidate('invalid_token');
        return service.validate('invalidtoken')
          .then((result: any) => {
            throw new Error('Promise was unexpectedly fulfilled. Result: ' + result);
          })
          .catch((e: Error) => {
            assert.equal(e.name, 'InvalidTokenError');
            assert.equal(e.message, 'Couldn\'t parse the given session jwt');
          });
      });

      it('should throw SessionNotFoundError if session not found', () => {
        mockValidate('session_notfound');
        return service.validate('validtoken')
          .then((result: any) => {
            throw new Error('Promise was unexpectedly fulfilled. Result: ' + result);
          })
          .catch((e: Error) => {
            assert.equal(e.name, 'SessionNotFoundError');
            assert.equal(e.message, 'sessionNotFound');
          });
      });

      it('should throw SessionExpiredError if session not found', () => {
        mockValidate('session_expired');
        return service.validate('validtoken')
          .then((result: any) => {
            throw new Error('Promise was unexpectedly fulfilled. Result: ' + result);
          })
          .catch((e: Error) => {
            assert.equal(e.name, 'SessionExpiredError');
            assert.equal(e.message, 'sessionExpired');
          });
      });

      it('should throw if ASAP key is invalid', () => {
        mockValidate('invalid_asap_key');
        return service.validate('validtoken')
          .then((result: any) => {
            throw new Error('Promise was unexpectedly fulfilled. Result: ' + result);
          })
          .catch((e: Error) => {
            assert.equal(e.message, 'Request failed with status code 401');
          });
      });

    });

  });
});
