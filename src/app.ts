import * as http from 'http';
import * as Koa from 'koa';
import Metrics from './lib/metrics';
import * as Router from 'koa-router';
import * as routing from './routing';
import controller from './controllers/index';
import createLogger from './lib/logger';
import MW from './middleware';
import config from './config';
const logger = createLogger({ level: 'debug' });
const app = new Koa();

app.on('error', (err: Error) => {
  logger.error({err}, 'Server error');
});

// Trust ELB SSH Proxy (micros)
app.proxy = true;

// initialise metrics collector and logger
const metricsInstance = new Metrics(process.env['MICROS_SERVICE'] || 'directory');
const metricLogger = logger.child({ subsystem: 'metric' });
metricsInstance.on('metric', (name: string, componentPath: any, data: any) => {
  metricLogger.info({type: name, componentPath, value: data.value}, '%s event on %s', name, componentPath);
});

// Load default middleware
app.use(MW({ logger, metricsInstance }));

// Add basic routing (healthcheck and index)
app.use(routing.basic(new Router(), controller));

// Create the server
http.createServer(app.callback()).listen(config.port, () => {
  logger.info('Service listening on %d', config.port);
});

// Make sure uncaught exceptions are handled and logged
const processLogger = logger.child({subsystem: 'process'});
process.on('unhandledRejection', (reason: any, p: any) => {
  processLogger.fatal({type: 'unhandledRejection'}, `Unhandled Rejection at: Promise ${p} reason ${reason}`);
});

process.on('uncaughtException', (err: Error) => {
  processLogger.fatal({err, type: 'uncaughtException'}, err);
});
