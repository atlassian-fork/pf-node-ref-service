/*globals it, describe, beforeEach, afterEach */

import * as assert from 'assert';
import index from './index';

describe('controllers/health', () => {
  it('should expose several functions', () => {
    assert.ok(index.index, 'no index function');
    assert.ok(index.health, 'no health function');
  });
});
