#!/bin/bash
set -e

GITREV="$(git rev-parse --short=10 HEAD)"
sed -e "s/%%VERSION%%/${GITREV}/" sd.yaml > ../pf-node-ref-service.sd.yaml
