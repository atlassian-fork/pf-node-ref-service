FROM node:6.4-slim
ENV NODE_ENV production
RUN cd $(npm root -g)/npm \
&& npm install fs-extra \
&& sed -i -e s/graceful-fs/fs-extra/ -e s/fs.rename/fs.move/ ./lib/utils/rename.js

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package.json /usr/src/app/
COPY typings.json /usr/src/app/

RUN npm install
RUN npm run postinstall

COPY . /usr/src/app

EXPOSE 8080

CMD [ "npm", "start" ]
